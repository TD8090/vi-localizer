window.Socialite = (function (window, document, undefined)
{
    'use strict';
    var uid = 0,
        instances = [
        ],
        networks = {
        },
        widgets = {
        },
        rstate = /^($|loaded|complete)/,
        euc = window.encodeURIComponent;
    var socialite = {
        settings: {
        },
        trim: function (str)
        {
            return str.trim ? str.trim()  : str.replace(/^\s+|\s+$/g, '');
        },
        hasClass: function (el, cn)
        {
            return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== - 1;
        },
        addClass: function (el, cn)
        {
            if (!socialite.hasClass(el, cn)) {
                el.className = (el.className === '') ? cn : el.className + ' ' + cn;
            }
        },
        removeClass: function (el, cn)
        {
            el.className = socialite.trim(' ' + el.className + ' '.replace(' ' + cn + ' ', ' '));
        },
        extendObject: function (to, from, overwrite)
        {
            for (var prop in from) {
                var hasProp = to[prop] !== undefined;
                if (hasProp && typeof from[prop] === 'object') {
                    socialite.extendObject(to[prop], from[prop], overwrite);
                } else if (overwrite || !hasProp) {
                    to[prop] = from[prop];
                }
            }
        },
        getElements: function (context, cn)
        {
            var i = 0,
                el = [
                ],
                gcn = !!context.getElementsByClassName,
                all = gcn ? context.getElementsByClassName(cn)  : context.getElementsByTagName('*');
            for (; i < all.length; i++) {
                if (gcn || socialite.hasClass(all[i], cn)) {
                    el.push(all[i]);
                }
            }
            return el;
        },
        getDataAttributes: function (el, noprefix, nostr)
        {
            var i = 0,
                str = '',
                obj = {
                },
                attr = el.attributes;
            for (; i < attr.length; i++) {
                var key = attr[i].name,
                    val = attr[i].value;
                if (val.length && key.indexOf('data-') === 0) {
                    if (noprefix) {
                        key = key.substring(5);
                    }
                    if (nostr) {
                        obj[key] = val;
                    } else {
                        str += euc(key) + '=' + euc(val) + '&';
                    }
                }
            }
            return nostr ? obj : str;
        },
        copyDataAttributes: function (from, to, noprefix, nohyphen)
        {
            var attr = socialite.getDataAttributes(from, noprefix, true);
            for (var i in attr) {
                to.setAttribute(nohyphen ? i.replace(/-/g, '_')  : i, attr[i]);
            }
        },
        createIframe: function (src, instance)
        {
            var iframe = document.createElement('iframe');
            iframe.style.cssText = 'overflow: hidden; border: none;';
            socialite.extendObject(iframe, {
                src: src,
                allowtransparency: 'true',
                frameborder: '0',
                scrolling: 'no'
            }, true);
            if (instance) {
                iframe.onload = iframe.onreadystatechange = function ()
                {
                    if (rstate.test(iframe.readyState || '')) {
                        iframe.onload = iframe.onreadystatechange = null;
                        socialite.activateInstance(instance);
                    }
                };
            }
            return iframe;
        },
        networkReady: function (name)
        {
            return networks[name] ? networks[name].loaded : undefined;
        },
        appendNetwork: function (network)
        {
            if (!network || network.appended) {
                return;
            }
            if (typeof network.append === 'function' && network.append(network) === false) {
                network.appended = network.loaded = true;
                socialite.activateAll(network);
                return;
            }
            if (network.script) {
                network.el = document.createElement('script');
                socialite.extendObject(network.el, network.script, true);
                network.el.async = true;
                network.el.onload = network.el.onreadystatechange = function ()
                {
                    if (rstate.test(network.el.readyState || '')) {
                        network.el.onload = network.el.onreadystatechange = null;
                        network.loaded = true;
                        if (typeof network.onload === 'function' && network.onload(network) === false) {
                            return;
                        }
                        socialite.activateAll(network);
                    }
                };
                document.body.appendChild(network.el);
            }
            network.appended = true;
        },
        removeNetwork: function (network)
        {
            if (!socialite.networkReady(network.name)) {
                return false;
            }
            if (network.el.parentNode) {
                network.el.parentNode.removeChild(network.el);
            }
            return !(network.appended = network.loaded = false);
        },
        reloadNetwork: function (name)
        {
            var network = networks[name];
            if (network && socialite.removeNetwork(network)) {
                socialite.appendNetwork(network);
            }
        },
        createInstance: function (el, widget)
        {
            var proceed = true,
                instance = {
                    el: el,
                    uid: uid++,
                    widget: widget
                };
            instances.push(instance);
            if (widget.process !== undefined) {
                proceed = (typeof widget.process === 'function') ? widget.process(instance)  : false;
            }
            if (proceed) {
                socialite.processInstance(instance);
            }
            instance.el.setAttribute('data-socialite', instance.uid);
            instance.el.className = 'socialite ' + widget.name + ' socialite-instance';
            return instance;
        },
        processInstance: function (instance)
        {
            var el = instance.el;
            instance.el = document.createElement('div');
            instance.el.className = el.className;
            socialite.copyDataAttributes(el, instance.el);
            if (el.nodeName.toLowerCase() === 'a' && !el.getAttribute('data-default-href')) {
                instance.el.setAttribute('data-default-href', el.getAttribute('href'));
            }
            var parent = el.parentNode;
            parent.insertBefore(instance.el, el);
            parent.removeChild(el);
        },
        activateInstance: function (instance)
        {
            if (instance && !instance.loaded) {
                instance.loaded = true;
                if (typeof instance.widget.activate === 'function') {
                    instance.widget.activate(instance);
                }
                socialite.addClass(instance.el, 'socialite-loaded');
                return instance.onload ? instance.onload(instance.el)  : null;
            }
        },
        activateAll: function (network)
        {
            if (typeof network === 'string') {
                network = networks[network];
            }
            for (var i = 0; i < instances.length; i++) {
                var instance = instances[i];
                if (instance.init && instance.widget.network === network) {
                    socialite.activateInstance(instance);
                }
            }
        },
        load: function (context, el, w, onload, process)
        {
            context = (context && typeof context === 'object' && context.nodeType === 1) ? context : document;
            if (!el || typeof el !== 'object') {
                socialite.load(context, socialite.getElements(context, 'socialite'), w, onload, process);
                return;
            }
            var i;
            if (/Array/.test(Object.prototype.toString.call(el))) {
                for (i = 0; i < el.length; i++) {
                    socialite.load(context, el[i], w, onload, process);
                }
                return;
            }
            if (el.nodeType !== 1) {
                return;
            }
            if (!w || !widgets[w]) {
                w = null;
                var classes = el.className.split(' ');
                for (i = 0; i < classes.length; i++) {
                    if (widgets[classes[i]]) {
                        w = classes[i];
                        break;
                    }
                }
                if (!w) {
                    return;
                }
            }
            var instance,
                widget = widgets[w],
                sid = parseInt(el.getAttribute('data-socialite'), 10);
            if (!isNaN(sid)) {
                for (i = 0; i < instances.length; i++) {
                    if (instances[i].uid === sid) {
                        instance = instances[i];
                        break;
                    }
                }
            } else {
                instance = socialite.createInstance(el, widget);
            }
            if (process || !instance) {
                return;
            }
            if (!instance.init) {
                instance.init = true;
                instance.onload = (typeof onload === 'function') ? onload : null;
                widget.init(instance);
            }
            if (!widget.network.appended) {
                socialite.appendNetwork(widget.network);
            } else {
                if (socialite.networkReady(widget.network.name)) {
                    socialite.activateInstance(instance);
                }
            }
        },
        activate: function (el, w, onload)
        {
            window.Socialite.load(null, el, w, onload);
        },
        process: function (context, el, w)
        {
            window.Socialite.load(context, el, w, null, true);
        },
        network: function (n, params)
        {
            networks[n] = {
                name: n,
                el: null,
                appended: false,
                loaded: false,
                widgets: {
                }
            };
            if (params) {
                socialite.extendObject(networks[n], params);
            }
        },
        widget: function (n, w, params)
        {
            params.name = n + '-' + w;
            if (!networks[n] || widgets[params.name]) {
                return;
            }
            params.network = networks[n];
            networks[n].widgets[w] = widgets[params.name] = params;
        },
        setup: function (params)
        {
            socialite.extendObject(socialite.settings, params, true);
        }
    };
    return socialite;
}) (window, window.document);
(function (window, document, Socialite, undefined)
{
    Socialite.setup({
        facebook: {
            lang: 'en_GB',
            appId: null
        },
        twitter: {
            lang: 'en'
        },
        googleplus: {
            lang: 'en-GB'
        }
    });
    Socialite.network('facebook', {
        script: {
            src: '//connect.facebook.net/{{language}}/all.js',
            id: 'facebook-jssdk'
        },
        append: function (network)
        {
            var fb = document.createElement('div'),
                settings = Socialite.settings.facebook,
                events = {
                    onlike: 'edge.create',
                    onunlike: 'edge.remove',
                    onsend: 'message.send'
                };
            fb.id = 'fb-root';
            document.body.appendChild(fb);
            network.script.src = network.script.src.replace('{{language}}', settings.lang);
            window.fbAsyncInit = function () {
                window.FB.init({
                    appId: settings.appId,
                    xfbml: true
                });
                for (var e in events) {
                    if (typeof settings[e] === 'function') {
                        window.FB.Event.subscribe(events[e], settings[e]);
                    }
                }
            };
        }
    });
    Socialite.widget('facebook', 'like', {
        init: function (instance)
        {
            var el = document.createElement('div');
            el.className = 'fb-like';
            Socialite.copyDataAttributes(instance.el, el);
            instance.el.appendChild(el);
            if (window.FB && window.FB.XFBML) {
                window.FB.XFBML.parse(instance.el);
            }
        }
    });
    Socialite.network('twitter', {
        script: {
            src: '//platform.twitter.com/widgets.js',
            id: 'twitter-wjs',
            charset: 'utf-8'
        },
        append: function ()
        {
            var notwttr = (typeof window.twttr !== 'object'),
                settings = Socialite.settings.twitter,
                events = [
                    'click',
                    'tweet',
                    'retweet',
                    'favorite',
                    'follow'
                ];
            if (notwttr) {
                window.twttr = (t = {
                    _e: [
                    ],
                    ready: function (f) {
                        t._e.push(f);
                    }
                });
            }
            window.twttr.ready(function (twttr)
            {
                for (var i = 0; i < events.length; i++) {
                    var e = events[i];
                    if (typeof settings['on' + e] === 'function') {
                        twttr.events.bind(e, settings['on' + e]);
                    }
                }
                Socialite.activateAll('twitter');
            });
            return notwttr;
        }
    });
    var twitterInit = function (instance)
    {
        var el = document.createElement('a');
        el.className = instance.widget.name + '-button';
        Socialite.copyDataAttributes(instance.el, el);
        el.setAttribute('href', instance.el.getAttribute('data-default-href'));
        el.setAttribute('data-lang', instance.el.getAttribute('data-lang') || Socialite.settings.twitter.lang);
        instance.el.appendChild(el);
    };
    var twitterActivate = function (instance)
    {
        if (window.twttr && typeof window.twttr.widgets === 'object' && typeof window.twttr.widgets.load === 'function') {
            window.twttr.widgets.load();
        }
    };
    Socialite.widget('twitter', 'share', {
        init: twitterInit,
        activate: twitterActivate
    });
    Socialite.widget('twitter', 'follow', {
        init: twitterInit,
        activate: twitterActivate
    });
    Socialite.widget('twitter', 'hashtag', {
        init: twitterInit,
        activate: twitterActivate
    });
    Socialite.widget('twitter', 'mention', {
        init: twitterInit,
        activate: twitterActivate
    });
    Socialite.widget('twitter', 'embed', {
        process: function (instance)
        {
            instance.innerEl = instance.el;
            if (!instance.innerEl.getAttribute('data-lang')) {
                instance.innerEl.setAttribute('data-lang', Socialite.settings.twitter.lang);
            }
            instance.el = document.createElement('div');
            instance.el.className = instance.innerEl.className;
            instance.innerEl.className = '';
            instance.innerEl.parentNode.insertBefore(instance.el, instance.innerEl);
            instance.el.appendChild(instance.innerEl);
        },
        init: function (instance)
        {
            instance.innerEl.className = 'twitter-tweet';
        },
        activate: twitterActivate
    });
    Socialite.network('googleplus', {
        script: {
            src: '//apis.google.com/js/plusone.js'
        },
        append: function (network)
        {
            if (window.gapi) {
                return false;
            }
            window.___gcfg = {
                lang: Socialite.settings.googleplus.lang,
                parsetags: 'explicit'
            };
        }
    });
    var googleplusInit = function (instance)
    {
        var el = document.createElement('div');
        el.className = 'g-' + instance.widget.gtype;
        Socialite.copyDataAttributes(instance.el, el);
        instance.el.appendChild(el);
        instance.gplusEl = el;
    };
    var googleplusEvent = function (instance, callback) {
        return (typeof callback !== 'function') ? null : function (data) {
            callback(instance.el, data);
        };
    };
    var googleplusActivate = function (instance)
    {
        var type = instance.widget.gtype;
        if (window.gapi && window.gapi[type]) {
            var settings = Socialite.settings.googleplus,
                params = Socialite.getDataAttributes(instance.el, true, true),
                events = [
                    'onstartinteraction',
                    'onendinteraction',
                    'callback'
                ];
            for (var i = 0; i < events.length; i++) {
                params[events[i]] = googleplusEvent(instance, settings[events[i]]);
            }
            window.gapi[type].render(instance.gplusEl, params);
        }
    };
    Socialite.widget('googleplus', 'one', {
        init: googleplusInit,
        activate: googleplusActivate,
        gtype: 'plusone'
    });
    Socialite.widget('googleplus', 'share', {
        init: googleplusInit,
        activate: googleplusActivate,
        gtype: 'plus'
    });
    Socialite.widget('googleplus', 'badge', {
        init: googleplusInit,
        activate: googleplusActivate,
        gtype: 'plus'
    });
    Socialite.network('linkedin', {
        script: {
            src: '//platform.linkedin.com/in.js'
        }
    });
    var linkedinInit = function (instance)
    {
        var el = document.createElement('script');
        el.type = 'IN/' + instance.widget.intype;
        Socialite.copyDataAttributes(instance.el, el);
        instance.el.appendChild(el);
        if (typeof window.IN === 'object' && typeof window.IN.parse === 'function') {
            window.IN.parse(instance.el);
            Socialite.activateInstance(instance);
        }
    };
    Socialite.widget('linkedin', 'share', {
        init: linkedinInit,
        intype: 'Share'
    });
    Socialite.widget('linkedin', 'recommend', {
        init: linkedinInit,
        intype: 'RecommendProduct'
    });
    Socialite.widget('linkedin', 'follow', {
        init: linkedinInit,
        intype: 'FollowCompany'
    });
}) (window, window.document, window.Socialite);
(function () {
    var s = window._socialite;
    if (/Array/.test(Object.prototype.toString.call(s))) {
        for (var i = 0, len = s.length; i < len; i++) {
            if (typeof s[i] === 'function') {
                s[i]();
            }
        }
    }
}) ();
(function ($) {
    $.fn.zclip = function (params) {
        if (typeof params == 'object' && !params.length) {
            var settings = $.extend({
                path: 'ZeroClipboard.swf',
                copy: null,
                beforeCopy: null,
                afterCopy: null,
                clickAfter: true,
                setHandCursor: true,
                setCSSEffects: true
            }, params);
            return this.each(function () {
                var o = $(this);
                if (o.is(':visible') && (typeof settings.copy == 'string' || $.isFunction(settings.copy))) {
                    ZeroClipboard.setMoviePath(settings.path);
                    var clip = new ZeroClipboard.Client();
                    if ($.isFunction(settings.copy)) {
                        o.bind('zClip_copy', settings.copy);
                    }
                    if ($.isFunction(settings.beforeCopy)) {
                        o.bind('zClip_beforeCopy', settings.beforeCopy);
                    }
                    if ($.isFunction(settings.afterCopy)) {
                        o.bind('zClip_afterCopy', settings.afterCopy);
                    }
                    clip.setHandCursor(settings.setHandCursor);
                    clip.setCSSEffects(settings.setCSSEffects);
                    clip.addEventListener('mouseOver', function (client) {
                        o.trigger('mouseenter');
                    });
                    clip.addEventListener('mouseOut', function (client) {
                        o.trigger('mouseleave');
                    });
                    clip.addEventListener('mouseDown', function (client) {
                        o.trigger('mousedown');
                        if (!$.isFunction(settings.copy)) {
                            clip.setText(settings.copy);
                        } else {
                            clip.setText(o.triggerHandler('zClip_copy'));
                        }
                        if ($.isFunction(settings.beforeCopy)) {
                            o.trigger('zClip_beforeCopy');
                        }
                    });
                    clip.addEventListener('complete', function (client, text) {
                        if ($.isFunction(settings.afterCopy)) {
                            o.trigger('zClip_afterCopy');
                        } else {
                            if (text.length > 500) {
                                text = text.substr(0, 500) + '...\n\n(' + (text.length - 500) + ' characters not shown)';
                            }
                            o.removeClass('hover');
                            alert(text_copied + '\n\n ' + text);
                        }
                        if (settings.clickAfter) {
                            o.trigger('click');
                        }
                    });
                    clip.glue(o[0], o.parent() [0]);
                    $(window).bind('load resize', function () {
                        clip.reposition();
                    });
                }
            });
        } else if (typeof params == 'string') {
            return this.each(function () {
                var o = $(this);
                params = params.toLowerCase();
                var zclipId = o.data('zclipId');
                var clipElm = $('#' + zclipId + '.zclip');
                if (params == 'remove') {
                    clipElm.remove();
                    o.removeClass('active hover');
                } else if (params == 'hide') {
                    clipElm.hide();
                    o.removeClass('active hover');
                } else if (params == 'show') {
                    clipElm.show();
                }
            });
        }
    };
}) (jQuery);
var ZeroClipboard = {
    version: '1.0.7',
    clients: {
    },
    moviePath: 'ZeroClipboard.swf',
    nextId: 1,
    $: function (thingy) {
        if (typeof (thingy) == 'string') thingy = document.getElementById(thingy);
        if (!thingy.addClass) {
            thingy.hide = function () {
                this.style.display = 'none';
            };
            thingy.show = function () {
                this.style.display = '';
            };
            thingy.addClass = function (name) {
                this.removeClass(name);
                this.className += ' ' + name;
            };
            thingy.removeClass = function (name) {
                var classes = this.className.split(/\s+/);
                var idx = - 1;
                for (var k = 0; k < classes.length; k++) {
                    if (classes[k] == name) {
                        idx = k;
                        k = classes.length;
                    }
                }
                if (idx > - 1) {
                    classes.splice(idx, 1);
                    this.className = classes.join(' ');
                }
                return this;
            };
            thingy.hasClass = function (name) {
                return !!this.className.match(new RegExp('\\s*' + name + '\\s*'));
            };
        }
        return thingy;
    },
    setMoviePath: function (path) {
        this.moviePath = path;
    },
    dispatch: function (id, eventName, args) {
        var client = this.clients[id];
        if (client) {
            client.receiveEvent(eventName, args);
        }
    },
    register: function (id, client) {
        this.clients[id] = client;
    },
    getDOMObjectPosition: function (obj, stopObj) {
        var info = {
            left: 0,
            top: 0,
            width: obj.offsetWidth ? obj.offsetWidth : obj.width,
            height: obj.offsetHeight ? obj.offsetHeight : obj.height
        };
        if (obj && (obj != stopObj)) {
            info.left += obj.offsetLeft;
            info.top += obj.offsetTop;
        }
        return info;
    },
    Client: function (elem) {
        this.handlers = {
        };
        this.id = ZeroClipboard.nextId++;
        this.movieId = 'ZeroClipboardMovie_' + this.id;
        ZeroClipboard.register(this.id, this);
        if (elem) this.glue(elem);
    }
};
ZeroClipboard.Client.prototype = {
    id: 0,
    ready: false,
    movie: null,
    clipText: '',
    handCursorEnabled: true,
    cssEffects: true,
    handlers: null,
    glue: function (elem, appendElem, stylesToAdd) {
        this.domElement = ZeroClipboard.$(elem);
        var zIndex = 99;
        if (this.domElement.style.zIndex) {
            zIndex = parseInt(this.domElement.style.zIndex, 10) + 1;
        }
        appendElem = document.getElementsByTagName('body') [0];
        var box = ZeroClipboard.getDOMObjectPosition(this.domElement, appendElem);
        this.div = document.createElement('div');
        this.div.className = 'zclip';
        this.div.id = 'zclip-' + this.movieId;
        $(this.domElement).data('zclipId', 'zclip-' + this.movieId);
        var style = this.div.style;
        style.position = 'absolute';
        style.left = '' + box.left + 'px';
        style.top = '' + box.top + 'px';
        style.width = '' + box.width + 'px';
        style.height = '' + box.height + 'px';
        style.zIndex = zIndex;
        if (typeof (stylesToAdd) == 'object') {
            for (addedStyle in stylesToAdd) {
                style[addedStyle] = stylesToAdd[addedStyle];
            }
        }
        appendElem.appendChild(this.div);
        $(this.div).html(this.getHTML(box.width, box.height));
    },
    getHTML: function (width, height) {
        var html = '';
        var flashvars = 'id=' + this.id + '&width=' + width + '&height=' + height;
        if (navigator.userAgent.match(/MSIE/)) {
            var protocol = location.href.match(/^https/i) ? 'https://' : 'http://';
            html += '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="' + protocol + 'download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="' + width + '" height="' + height + '" id="' + this.movieId + '" align="middle"><param name="allowScriptAccess" value="always" /><param name="allowFullScreen" value="false" /><param name="movie" value="' + ZeroClipboard.moviePath + '" /><param name="loop" value="false" /><param name="menu" value="false" /><param name="quality" value="best" /><param name="bgcolor" value="#ffffff" /><param name="flashvars" value="' + flashvars + '"/><param name="wmode" value="transparent"/></object>';
        } else {
            html += '<embed id="' + this.movieId + '" src="' + ZeroClipboard.moviePath + '" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="' + width + '" height="' + height + '" name="' + this.movieId + '" align="middle" allowScriptAccess="always" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="' + flashvars + '" wmode="transparent" />';
        }
        return html;
    },
    hide: function () {
        if (this.div) {
            this.div.style.left = '-2000px';
        }
    },
    show: function () {
        this.reposition();
    },
    destroy: function () {
        if (this.domElement && this.div) {
            this.hide();
            this.div.innerHTML = '';
            var body = document.getElementsByTagName('body') [0];
            try {
                body.removeChild(this.div);
            } catch (e) {
            }
            this.domElement = null;
            this.div = null;
        }
    },
    reposition: function (elem) {
        if (elem) {
            this.domElement = ZeroClipboard.$(elem);
            if (!this.domElement) this.hide();
        }
        if (this.domElement && this.div) {
            var box = ZeroClipboard.getDOMObjectPosition(this.domElement);
            var style = this.div.style;
            style.left = '' + box.left + 'px';
            style.top = '' + box.top + 'px';
        }
    },
    setText: function (newText) {
        this.clipText = newText;
        if (this.ready) {
            this.movie.setText(newText);
        }
    },
    addEventListener: function (eventName, func) {
        eventName = eventName.toString().toLowerCase().replace(/^on/, '');
        if (!this.handlers[eventName]) {
            this.handlers[eventName] = [
            ];
        }
        this.handlers[eventName].push(func);
    },
    setHandCursor: function (enabled) {
        this.handCursorEnabled = enabled;
        if (this.ready) {
            this.movie.setHandCursor(enabled);
        }
    },
    setCSSEffects: function (enabled) {
        this.cssEffects = !!enabled;
    },
    receiveEvent: function (eventName, args) {
        eventName = eventName.toString().toLowerCase().replace(/^on/, '');
        switch (eventName) {
            case 'load':
                this.movie = document.getElementById(this.movieId);
                if (!this.movie) {
                    var self = this;
                    setTimeout(function () {
                        self.receiveEvent('load', null);
                    }, 1);
                    return;
                }
                if (!this.ready && navigator.userAgent.match(/Firefox/) && navigator.userAgent.match(/Windows/)) {
                    var self = this;
                    setTimeout(function () {
                        self.receiveEvent('load', null);
                    }, 100);
                    this.ready = true;
                    return;
                }
                this.ready = true;
                try {
                    this.movie.setText(this.clipText);
                } catch (e) {
                }
                try {
                    this.movie.setHandCursor(this.handCursorEnabled);
                } catch (e) {
                }
                break;
            case 'mouseover':
                if (this.domElement && this.cssEffects) {
                    this.domElement.addClass('hover');
                    if (this.recoverActive) {
                        this.domElement.addClass('active');
                    }
                }
                break;
            case 'mouseout':
                if (this.domElement && this.cssEffects) {
                    this.recoverActive = false;
                    if (this.domElement.hasClass('active')) {
                        this.domElement.removeClass('active');
                        this.recoverActive = true;
                    }
                    this.domElement.removeClass('hover');
                }
                break;
            case 'mousedown':
                if (this.domElement && this.cssEffects) {
                    this.domElement.addClass('active');
                }
                break;
            case 'mouseup':
                if (this.domElement && this.cssEffects) {
                    this.domElement.removeClass('active');
                    this.recoverActive = false;
                }
                break;
        }
        if (this.handlers[eventName]) {
            for (var idx = 0, len = this.handlers[eventName].length; idx < len; idx++) {
                var func = this.handlers[eventName][idx];
                if (typeof (func) == 'function') {
                    func(this, args);
                } else if ((typeof (func) == 'object') && (func.length == 2)) {
                    func[0][func[1]](this, args);
                } else if (typeof (func) == 'string') {
                    window[func](this, args);
                }
            }
        }
    }
};
function getFlashVersion() {
    try {
        try {
            var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
            try {
                axo.AllowScriptAccess = 'always';
            }
            catch (e) {
                return '6,0,0';
            }
        } catch (e) {
        }
        return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/) [1];
    } catch (e) {
        try {
            if (navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
                return (navigator.plugins['Shockwave Flash 2.0'] || navigator.plugins['Shockwave Flash']).description.replace(/\D+/g, ',').match(/^,?(.+),?$/) [1];
            }
        } catch (e) {
        }
    }
    return '0,0,0';
}
var flashVersion = getFlashVersion().split(',').shift();
function sentencecase(a) {
    a = a.toLowerCase();
    var b = true;
    var c = '';
    for (var d = 0; d < a.length; d++) {
        var e = a.charAt(d);
        if (/\.|\!|\?|\n|\r/.test(e)) {
            b = true;
        } else if ($.trim(e) != '' && b == true) {
            e = e.toUpperCase();
            b = false;
        }
        c += e;
    }
    return c;
}
function alternatingcase(a) {
    a = a.toLowerCase();
    var b = '';
    for (var c = 0; c < a.length; c++) {
        var d = a.charAt(c);
        if (c % 2) {
            b += d.toUpperCase();
        } else {
            b += d;
        }
    }
    return b;
}
function ucfirst(a) {
    var b = a.charAt(0).toUpperCase();
    return b + a.substr(1);
}
function ucwords(a) {
    return (a + '').replace(/^(\S)|\s+(\S)/g, function (a) {
        return a.toUpperCase();
    });
}
$(document).ready(function () {
    var a = 'None';
    $('#upper').click(function () {
        $('#content').val($('#content').val().toUpperCase());
        ga('send', 'event', 'Convert', 'Upper', '', $('#content').val().split(' ').length);
        a = 'Upper';
        return false
    });
    $('#lower').click(function () {
        $('#content').val($('#content').val().toLowerCase());
        ga('send', 'event', 'Convert', 'Lower', '', $('#content').val().split(' ').length);
        a = 'Lower';
        return false
    });
    $('#capitalized').click(function () {
        $('#content').val(ucwords($('#content').val().toLowerCase()));
        ga('send', 'event', 'Convert', 'Capitalized', '', $('#content').val().split(' ').length);
        a = 'Capitalized';
        return false
    });
    $('#sentence').click(function () {
        $('#content').val(sentencecase($('#content').val()));
        ga('send', 'event', 'Convert', 'Sentence', '', $('#content').val().split(' ').length);
        a = 'Sentence';
        return false
    });
    $('#alternating').click(function () {
        $('#content').val(alternatingcase($('#content').val()));
        ga('send', 'event', 'Convert', 'Alternating', '', $('#content').val().split(' ').length);
        a = 'Alternating';
        return false
    });
    $('#download').click(function () {
        ga('send', 'event', 'Download', a, '', $('#content').val().split(' ').length);
    });
    var b = $('#content').attr('data-placeholder');
    if ($('#content').val() == '') {
        $('#content').val(b)
    }
    function calc_counts() {
        $('.char_count').text($('#content').val().length);
        $('.word_count').text($.trim($('#content').val()).replace(/\s+/gi, ' ').split(' ').length);
    }
    $('#content').focus(function () {
        if ($(this).val().toLowerCase() == b.toLowerCase()) {
            $(this).val('')
        }
        calc_counts();
    });
    $('#content').blur(function () {
        if ($(this).val() == '') {
            $(this).val(b)
        }
        calc_counts();
    });
    $('#content').keyup(function () {
        calc_counts();
    });
    calc_counts();
    Socialite.setup({
        facebook: {
            appId: 1395980653967399
        }
    });
    Socialite.load();
    if (flashVersion >= 10) {
        $('#copy').show();
        $('#copy').zclip({
            path: '/assets/swf/ZeroClipboard.swf',
            copy: function () {
                return $('#content').val();
            },
            beforeCopy: function () {
                this.copy = $('#content').val();
            },
            afterCopy: function () {
                alert(text_copied);
            }
        });
    } else if (window.clipboardData && clipboardData.setData) {
        $('#copy').show();
        $('#copy').click(function () {
            clipboardData.setData('text', $('#content').val());
            return false;
        });
    }
});
